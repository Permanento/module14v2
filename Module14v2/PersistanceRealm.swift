//
//  PersistanceRealm.swift
//  Module14v2
//
//  Created by Сергей Гринько on 19.03.2021.
//

import Foundation


import RealmSwift

class Task: Object{
    @objc dynamic var taskDescription = ""
}

class PersistanceRealm {
    static let shared = PersistanceRealm()
    
    private let realm = try! Realm()
    
    func addTask(description:String){
        let task = Task()
        task.taskDescription = description
        try! realm.write{
            realm.add(task)
        }
    }
    
    func getTasks() -> [Task] {
        var outputArr:[Task] = []
        let realmObjects = realm.objects(Task.self)
        
        for object in realmObjects{
            outputArr.append(object)
        }
        
        
        return outputArr
    }
    
    func getTaskByIndex(index: Int) -> Task{
        let tasks = realm.objects(Task.self)
        
        return tasks[index]
    }
    
    func test(){
        let realmObjects = realm.objects(Task.self)
        
        for object in realmObjects{
            print(object.taskDescription)
        }
    }
    
    func deleteTask(index: Int){
        let tasks = realm.objects(Task.self)
        
        let taskToDelete = tasks[index]
        
        try! realm.write{
            realm.delete(taskToDelete)
        }
    }
}
